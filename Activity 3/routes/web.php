<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function ()
{
	return "Hello World"; 
});

Route::get('/helloworld', function() 
{
	return view("helloworld");
});

Route::get('test/', 'TestController@test');

Route::get('/askme', function() 
{
	return view('whoami');
});
Route::post('/whoami', 'WhatsMyNameController@index');

Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@onLogin');
Route::get('/login2', function()
{
	return view('login2');
});
Route::get('/login3', function()
{
	return view('login3');
});
Route::post('/doLogin', 'Login3Controller@onLogin');

// Customer routes
Route::get('/addCustomer', 'CustomerController@showAddCustomerForm');
Route::post('/addCustomer', 'CustomerController@doAddCustomer');

// Order routes
Route::get('/newOrder', 'OrderController@showNewOrderForm');
Route::post('/addOrder', 'OrderController@doAddOrder');