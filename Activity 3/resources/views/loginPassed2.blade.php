@extends('layouts.appmaster')
@section('title', 'Login Passed')
@section('content')
	@if ($model->getUsername() == 'ChadChadwick')
		<h3>Chad, you have logged in successfully.</h3>
	@else
		<h3>Someone besides Chad logged in successfully.</h3>
	@endif
@endsection