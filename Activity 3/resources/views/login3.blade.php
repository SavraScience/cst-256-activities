@extends('layouts.appmaster')
@section('title', 'Login Page')
@section('content')
	<form method="post" action="doLogin">
		<?php echo csrf_field(); ?>
		<h2>Login</h2>
		<table>
			<tr>
				<td>User Name: </td>
				<td>
					<input type="text" name="username" />
					<?php 
						echo '<p style="color: red;">' . $errors->first('username') . "</p>";
					?>
				</td>
			</tr>
			<tr>
				<td>Password: </td>
				<td>
					<input type="password" name="password" />
					<?php 
						echo '<p style="color: red;">' . $errors->first('password') . "</p>";
					?>					
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Login" />
				</td>
			</tr>								
		</table>
	</form>
@endsection