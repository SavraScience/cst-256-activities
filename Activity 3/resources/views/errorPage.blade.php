@extends('layouts.appmaster')
@section('title', 'Internal Server Error')
@section('content')
	<h2>Internal Server Error</h2>
	<p>We're sorry, something went wrong while processing your request...</p>
@endsection