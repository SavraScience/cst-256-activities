@extends('layouts.appmaster')
@section('title', 'Order Confirmation')
@section('content')
	<h2>Order Confirmation</h2>
	<p>Thanks for your order of {{ $product->getName() }}, {{ $customer->getFirstName() }}</p>
	<a href="newOrder">Place another order</a>
@endsection