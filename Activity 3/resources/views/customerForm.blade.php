@extends('layouts.appmaster')
@section('title', 'Add Customer')
@section('content')
	<form method="post" action="addCustomer">
		{{ csrf_field() }}
		<h2>New Customer</h2>
		<table id="form-add-customer">
			<tr>
				<td>First Name: </td>
				<td>
					<input type="text" name="firstName" />
					@if ($errors->has('firstName'))
						<p style="color: red;">{{ $errors->first('firstName') }}</p>
					@endif
				</td>
			</tr>
			<tr>
				<td>Last Name: </td>
				<td>
					<input type="text" name="lastName" />
					@if ($errors->has('lastName'))
						<p style="color: red;">{{ $errors->first('lastName') }}</p>
					@endif					
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Add" />
				</td>
			</tr>								
		</table>
	</form>
@endsection