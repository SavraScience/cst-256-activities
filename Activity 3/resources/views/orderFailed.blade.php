@extends('layouts.appmaster')
@section('title', 'Order Confirmation')
@section('content')
	<h2>Order Failed</h2>
	<p>Sorry, {{ $customer->getFirstName() }}, but your order failed to process. Please try again...</p>
	<a href="newOrder">Try again</a>
@endsection