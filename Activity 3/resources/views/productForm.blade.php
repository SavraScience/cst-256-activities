@extends('layouts.appmaster')
@section('title', 'Order Product')
@section('content')
	<form method="post" action="addOrder">
		{{ csrf_field() }}
		<h2>New Order</h2>
		<table id="form-add-order">
			<tr>
				<td>Who are you? </td>
				<td>
					<select name="customerId">
						@foreach($customers as $customer)
							@php
								$firstName = $customer->getFirstName();
								$lastName = $customer->getLastName();
							@endphp
							<option value="{{ $customer->getId() }}">{{ 
								$firstName . " " . $lastName
							 }}</option>
						@endforeach
					</select>
					@if ($errors->has('customerId'))
						<p style="color: red;">{{ $errors->first('customerId') }}</p>
					@endif					
				</td>
			</tr>
			<tr>
				<td>Product Name: </td>
				<td>
					<input type="text" name="productName" />
					@if ($errors->has('productName'))
						<p style="color: red;">{{ $errors->first('productName') }}</p>
					@endif					
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Order" />
				</td>
			</tr>								
		</table>
	</form>
@endsection