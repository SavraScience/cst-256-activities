<?php

namespace App\Services\Data;

use App\Models\ProductModel;
use App\Services\Utility\DatabaseException;
use Illuminate\Support\Facades\Log;
use PDO;
use PDOException;

/**
 *
 * @author djsav
 *        
 */
class OrderDAO {
	
	// CONSTANTS
	private static $TBL = "order";
	
	// INSTANCE VARIABLES
	/** @var $db PDO */
	private $db = null;
	
	// CONSTRUCTOR
	public function __construct(PDO $db)
	{
		// debugging
		Log::debug('Calling OrderDAO::__construct()...');
		$this->db = $db;

		// debugging
		Log::debug('Exiting OrderDAO::__construct()...');
	}
	
	public function __destruct()
	{
		$this->db = null;
	}
	
	public function create(ProductModel $product)
	{
		// debugging
		Log::debug('Calling OrderDAO::create()...');
		
		// initialize variables
		$newId = 0;
		
		// get table name
		$table = self::$TBL;
	
		// extract variables from $product
		$productName = $product->getName();
		$customerId = $product->getCustomerId();
		
		// generate SQL Statement
		$sql = <<<ML
			INSERT INTO `$table`
				(product, customer_id)
			VALUES
				(:product, :customerId)
ML;
		try
		{
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam('product', $productName);
			$stmt->bindParam('customerId', $customerId);
			$inserted = $stmt->execute();
			
			if ($inserted) $newId = $this->db->lastInsertId();
			
			$stmt = null;
			
		}
		catch (PDOException $e)
		{
			// throw an error
			Log::debug('PDOException in CustomerDAO::create()...');
			throw new DatabaseException("Couldn't add customer to the database");
		}
		
		return $newId;
		
		// debugging
		Log::debug('Exiting OrderDAO::create()...');
	}
}

