<?php

namespace App\Services\Data;

use \Exception;
use \PDO;
use \PDOException;
use \PDOStatement;
use App\Services\Utility\DatabaseException;
use Illuminate\Support\Facades\Log;
use App\Models\CustomerModel;

/**
 *
 * @author djsav
 *        
 */
class CustomerDAO {

	// CONSTANTS
	private static $TBL = "customer";
	private static $FLD_PK = "id";
	
	// INSTANCE VARIABLES
	/** @var $db PDO */
	private $db = null;
	
	// CONSTRUCTOR
	public function __construct(PDO $db)
	{
		// debugging
		Log::debug('Calling CustomerDAO::__construct()...');
		$this->db = $db;
		//$this->db->errorCode()
		// debugging
		Log::debug('Exiting CustomerDAO::__construct()...');
	}
	
	public function __destruct()
	{
		$this->db = null;
	}
	
	public function create(string $firstName, string $lastName)
	{
		// debugging
		Log::debug('Calling CustomerDAO::create()...');
		
		// intialize variables
		$newId = 0;
		
		// get table name
		$table = self::$TBL;
		
		// generate SQL Statement
		$sql = <<<ML
			INSERT INTO `$table`
				(first_name, last_name)
			VALUES
				(:firstName, :lastName)
			;
ML;
		try
		{
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam('firstName', $firstName);
			$stmt->bindParam('lastName', $lastName);
			if ($stmt->execute())
			{
				$newId = $this->db->lastInsertId();
			}
		}
		catch (PDOException $e)
		{
			// throw an error
			Log::debug('PDOException in CustomerDAO::create()...');
			throw new DatabaseException("Couldn't add customer to the database");
		}
		finally
		{
			// close connection
			if (isset($stmt)) $stmt = null;
		}
		
		// debugging
		Log::debug('Exiting CustomerDAO::create()...');
		
		return $newId;
	}
	
	public function findAll()
	{
		// debugging
		Log::debug('Calling CustomerDAO::findAll()...');
		
		// initialize variables
		$customers = array();
		
		// get table name
		$table = self::$TBL;
		
		// generate SQL Statement
		$sql = <<<ML
			SELECT * 
			FROM `$table`
			;
ML;
		try
		{
			// execute the query
			$rs = $this->db->query($sql);
			
			foreach ($rs as $row)
			{
				// get row values
				$id = $row['id'];
				$firstName = $row['first_name'];
				$lastName = $row['last_name'];
				
				// instanitate a CustomerModel
				$customer = new CustomerModel($id, $firstName, $lastName);
				
				// add the newly-instantiated customer to an array
				array_push($customers, $customer);
			}
		}
		catch (PDOException $e)
		{
			// throw an error
			Log::debug('PDOException in CustomerDAO::create()...');
			throw new DatabaseException("Couldn't add customer to the database");
		}
		finally
		{
			// close connections
			if (isset($rs)) $rs = null;
			if (isset($stmt)) $stmt = null;
		}
		
		// debugging
		Log::debug('Exiting CustomerDAO::findAll()...');
		
		return $customers;
	}
	
	public function findById($id)
	{
		// debugging
		Log::debug('Calling CustomerDAO::findById()...');
		
		// initialize variables
		$customer = null;
		
		// get table name
		$table = self::$TBL;
		$fieldId = self::$FLD_PK;
		
		// generate SQL Statement
		$sql = <<<ML
			SELECT *
			FROM `$table`
			WHERE $fieldId = :id
			;
ML;
		try
		{
			//$rs = $this->db->query($sql);
			/** @var $stmt \PDOStatement */
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam('id', $id);
			if ($stmt->execute())
			{
				// get the first row
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				
				// get row values
				$id = $row['id'];
				$firstName = $row['first_name'];
				$lastName = $row['last_name'];
				
				// instanitate a CustomerModel
				$customer = new CustomerModel($id, $firstName, $lastName);
			}
		}
		catch (PDOException $e)
		{
			// throw an error
			Log::debug('PDOException in CustomerDAO::findById()...');
			throw $e;
			throw new DatabaseException("Couldn't get customer");
		}
		finally
		{
			// close connections
			if (isset($stmt)) $stmt = null;
		}
		
		// debugging
		Log::debug('Exiting CustomerDAO::findById()...');
		
		return $customer;
	}
}

