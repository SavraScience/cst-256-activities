<?php

namespace App\Services\Utility;

use \PDO;

class DbConnectionManager {
	
	/**
	 * Returns a database connection
	 * 
	 * @return PDO
	 */
	public static function getConnection()
	{
		// Configure the database
		$servername 	= config("database.connections.mysql.host");
		$port 			= config("database.connections.mysql.port");
		$username 		= config("database.connections.mysql.username");
		$password 		= config("database.connections.mysql.password");
		$dbname 		= config("database.connections.mysql.database");
		
		// Get a new connection and set its attributes
		$db = new PDO(
			"mysql:host=$servername;port=$port;dbname=$dbname",
			$username,
			$password
		);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		return $db;
	}
}