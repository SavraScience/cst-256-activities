<?php

namespace App\Services\Business;

use App\Models\ProductModel;
use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;
use App\Services\Utility\DbConnectionManager;
use PDOException;

class OrderService {

	// INJECTED SERVICES
	private $orderDataSvc;
	private $customerDataSvc;
	
	/**
	 * @param OrderDAO $orderDataSvc
	 */
	public function __construct(OrderDAO $orderDataSvc, CustomerDAO $custDataSvc)
	{
		// inject services
		$this->orderDataSvc = $orderDataSvc;
		$this->customerDataSvc = $custDataSvc;
	}
	
	/**
	 * @param ProductModel $product
	 * @return number|string
	 */
	//public function createOrder(ProductModel $product)
	public function createOrder(string $firstName, string $lastName, string $productName)
	{
		// initialize variables
		$newOrderId = null;
		
		// get a database connection
		$db = DbConnectionManager::getConnection();
		
		// set the service's data service's database connection
		$this->orderDataSvc = new OrderDAO($db);
		$this->customerDataSvc = new CustomerDAO($db);
		
		// transaction
		try
		{
			$db->beginTransaction();
			
			// first, add a new customer
			$newCustomerId = $this->customerDataSvc->create($firstName, $lastName);
			
			//$newCustomerId = 0; <- Uncommenting this code causes the whole transaction
			//to fail and be rolled back
			
			if ($newCustomerId > 0)
			{
				// next, create a new product model
				$product = new ProductModel(0, $productName, $newCustomerId);
				
				// finally, attempt to add the new product
				$newOrderId = $this->orderDataSvc->create($product);
				
				$db->commit();
			}
			else
			{
				throw new PDOException("Couldn't add customer to database");
			}
		}
		catch (PDOException $ex)
		{
			$db->rollBack();
		}
		// end transaction
		
		return $newOrderId;
	}
}

