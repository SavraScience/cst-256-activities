<?php

namespace App\Services\Business;

use App\Models\UserModel;
use App\Services\Data\SecurityDAO;
use App\Services\Utility\DbConnectionManager;
use Illuminate\Support\Facades\Log;
use \PDO;

class SecurityService
{
	public function authenticate(UserModel $user)
	{
		Log::info("Entering SecurityService.login()");
		
		// Get a connection to the database
		$db = DbConnectionManager::getConnection();
		
		// Create a Security Service DAO with this connection and attempt to locate user
		$service = new SecurityDAO($db);
		$flag = $service->findByUser($user);
		
		// Close the Database connection
		$db = null;
		
		// Debugging
		Log::info("Exit SecurityService.login() with " . $flag);
		
		return $flag;
	}
}

