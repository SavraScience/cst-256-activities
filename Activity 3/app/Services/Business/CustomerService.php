<?php

namespace App\Services\Business;

use App\Models\CustomerModel;
use App\Services\Data\CustomerDAO;
use Illuminate\Support\Facades\Log;
use PDO;
use App\Services\Utility\DbConnectionManager;

class CustomerService {

	// INJECTED SERVICES
	private $dataService;
	
	// CONSTRUCTOR
	//
	public function __construct(CustomerDAO $dataService) 
	{
		// inject services
		$this->dataService = $dataService;
	}
	//
	
	/**
	 * @return CustomerModel[]
	 */
	public function getAll()
	{
		return $this->dataService->findAll();
	}
	
	/**
	 * @param mixed $id
	 * @return NULL|CustomerModel
	 */
	public function get($id)
	{
		return $this->dataService->findById($id);
	}
	
	/**
	 * @param CustomerModel $newCustomer
	 */
	public function addCustomer(CustomerModel $newCustomer)
	{
		// debugging
		Log::debug('Calling CustomerService::addCustomer()...');
		
		// extract variables from customer model
		$firstName = $newCustomer->getFirstName();
		$lastName = $newCustomer->getLastName();
		
		// attempt to insert new customer into database
		Log::debug("Attempting to add new customer $firstName $lastName to the database");	
		$newId = $this->dataService->create(
			$firstName,
			$lastName
		);
		
		// debugging
		Log::debug('Exiting CustomerService::addCustomer()...');
		
		return $newId;
	}
}

