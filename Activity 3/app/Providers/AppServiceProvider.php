<?php

namespace App\Providers;

use App\Services\Business\CustomerService;
use App\Services\Data\CustomerDAO;
use App\Services\Utility\DbConnectionManager;
use Illuminate\Support\ServiceProvider;
use PDO;
use App\Http\Controllers\CustomerController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	// dependency resolvers
    	
    	// -- CustomerController
    	
    	$this->app->bind(CustomerController::class, function ($app) {
    		return new CustomerController($app->make(CustomerService::class));
    	});
    	
    	// -- Customer Service
    	$this->app->bind(CustomerService::class, function ($app) {
    		return new CustomerService($app->make(CustomerDAO::class));
    	});
    	
    	// -- Customer DAO
    	$this->app->bind(CustomerDAO::class, function ($app) {
    		
    		// instantiate the CustomerDAO
    		return new CustomerDAO($app->make(PDO::class));
    		
    	});
    	
    	// -- Database Connection
    		$this->app->bind(PDO::class, function ($app) {
    			
    			// Database Configuration information
    			$servername 	= config("database.connections.mysql.host");
    			$port 			= config("database.connections.mysql.port");
    			$username 		= config("database.connections.mysql.username");
    			$password 		= config("database.connections.mysql.password");
    			$dbname 		= config("database.connections.mysql.database");
    			
    			// Get a new connection and set its attributes
    			$db = new PDO(
    					"mysql:host=$servername;port=$port;dbname=$dbname",
    					$username,
    					$password
    					);
    			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    			
    			//return DbConnectionManager::getConnection();
    			return $db;
    		});
    	
    	// singletons
    	/*
    	$this->app->singleton(CustomerService::class, function($app) {
    		return new CustomerService($app->make(CustomerDAO::class));
    	});
    	*/
    }
}
