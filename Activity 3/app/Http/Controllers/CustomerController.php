<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\CustomerModel;
use App\Services\Business\CustomerService;

class CustomerController extends Controller
{
	// INJECTED SERVICES
	private $customerSvc;
	
	// CONSTRUCTOR
	/**
	 * @param CustomerService $customerSvc
	 */
	function __construct(CustomerService $customerSvc)
	{
		// inject services
		$this->customerSvc = $customerSvc;
	}
	
	/**
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
    function showAddCustomerForm()
    {
    	// show the customer form view/page
    	return view('customerForm');
    }
    
    function doAddCustomer(Request $request)
    {
    	// validate form data
    	$this->validateNewCustomerForm($request);
    	
    	Log::debug('Customer form validated');
    	
    	try
    	{
	    	// populate a new customer model
	    	$newCustomer = new CustomerModel(
	    		0,
	    		$request->input('firstName'),
	    		$request->input('lastName')
	    	);
    	
	    	Log::debug("New customer {$newCustomer->getFirstName()} {$newCustomer->getLastName()} instantiated!");
	    	
	    	// add customer to the database
    		$this->customerSvc->addCustomer($newCustomer);
    		
    		Log::debug('Customer should have been added to the DB');
    	}
    	catch (Exception $ex)
    	{
    		throw $ex;
    	}
    	
    	// redirect to customer form
    	return back();
    }
    	
    function validateNewCustomerForm(Request $request)
    {
    	$rules = [
    		'firstName' => 'required',
    		'lastName' 	=> 'required'
    	];
    	
    	$this->validate($request, $rules);
    }
}
