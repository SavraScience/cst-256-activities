<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Models\ProductModel;
use App\Services\Business\CustomerService;
use App\Services\Business\OrderService;

class OrderController extends Controller
{
	private $customerSvc;
	private $orderSvc;
	
    public function __construct(OrderService $orderSvc, CustomerService $customerSvc)
    {
    	$this->orderSvc = $orderSvc;
    	$this->customerSvc = $customerSvc;
    }
    
    public function showNewOrderForm()
    {
    	// get all existing customers
    	$customers = $this->customerSvc->getAll();
    	
    	return view('productForm')->with('customers', $customers);
    }
    
    public function doAddOrder(Request $request)
    {
    	// validate user input
    	$this->validateOrderForm($request);
    	
    	try
    	{
	    	// get user input
	    	$customerId = $request->input('customerId');
	    	$productName = $request->input('productName');
	    	
	    	// get the selected customer from the database
	    	$customer = $this->customerSvc->get($customerId);
	    	
	    	// add the order to the database
	    	$productId = $this->orderSvc->createOrder(
	    			$customer->getFirstName(), 
	    			$customer->getLastName(), 
	    			$productName
	    	);
    	}
    	catch (Exception $ex)
    	{
    		throw $ex;
    		return view('errorPage');
    	}
    	
    	if ($productId > 0)
    	{
    		// order created
    		// -- recreate the product to pass to view
    		$product = new ProductModel($productId, $productName, $customerId);
    		
    		return view('orderConfirmation')
    			->with([
    				'customer' 	=> $customer,
    				'product' 	=> $product
    			]
    		);
    	}
    	else
    	{
    		// order insertion failed
    		return view('orderFailed')->with('customer', $customer);
    	}
    }
    
    public function validateOrderForm(Request $request)
    {
    	$rules = [
    		'customerId' => 'required',
    		'productName' => 'required'
    	];
    	
    	$this->validate($request, $rules);
    }
}
