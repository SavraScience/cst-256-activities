<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Services\Business\SecurityService;

class Login3Controller extends Controller
{
    public function login()
    {
    	return view('login');
    }
    
    public function onLogin(Request $request)
    {
    	// validate the form
    	$this->validateForm($request);
    	
    	// get user input
    	$username = $request->input('username');
    	$password = $request->input('password');
    	
    	// create a new UserModel
    	$user = new UserModel(null, $username, $password);
    	
    	// Call Security Business Service
    	$service = new SecurityService();
    	$status = $service->authenticate($user);
    	
    	if ($status)
    	{
    		$data = ['model' => $user];
    		return view('loginPassed2')->with($data);
    	}
    	else
    	{
    		return view('loginFailed');
    	}
    	
    	return '';
    }
    
    private function validateForm(Request $request)
    {
    	$rules = [
    		'username' => 'required|between:4,15|Alpha',
    		'password' => 'required|between:4,10'
    	];
    	
    	$this->validate($request, $rules);
    }
}
