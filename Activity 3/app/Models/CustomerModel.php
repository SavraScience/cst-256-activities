<?php

namespace App\Models;

/**
 *
 * @author djsav
 *        
 */
class CustomerModel {

	// CLASS VARIABLES
	private $id;
	private $firstName;
	private $lastName;
	
	// NON-EMPTY CONSTRUCTOR
	public function __construct($id, string $firstName, string $lastName) 
	{
		$this->id = $id;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
	}
	
	// PUBLIC ACCESSOR METHODS (PROPERTIES)
	public function getId()
	{
		return $this->id;
	}
	
	public function getFirstName()
	{
		return $this->firstName;
	}
	
	public function getLastName()
	{
		return $this->lastName;
	}
}

