<?php

namespace App\Models;

/**
 *
 * @author djsav
 *        
 */
class ProductModel {

	private $id;
	private $productName;
	private $customerId;
	
	public function __construct($id, string $productName, $customerId) 
	{
		$this->id = $id;
		$this->productName = $productName;
		$this->customerId = $customerId;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getName() : string
	{
		return $this->productName;
	}
	
	public function getCustomerId()
	{
		return $this->customerId;
	}
}

