<!DOCTYPE HTML>
<html>
	<head>
		<title>What's My Name?</title>
	</head>
	<body>
		<form method="post" action="login">
			<?php echo csrf_field(); ?>
			<h2>Login</h2>
			<table>
				<tr>
					<td>User Name: </td>
					<td><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td>Password: </td>
					<td><input type="password" name="password" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Login" />
					</td>
				</tr>								
			</table>
		</form>
	</body>
</html>