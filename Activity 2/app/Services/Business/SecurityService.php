<?php

namespace App\Services\Business;

use App\Models\UserModel;
use App\Services\Data\SecurityDAO;
use Illuminate\Support\Facades\Log;
use \PDO;

class SecurityService
{
	public function authenticate(UserModel $user)
	{
		Log::info("Entering SecurityService.login()");
		
		// Configure the database
		$servername = config("database.connections.mysql.host");
		$port = config("database.connections.mysql.port");
		$username = config("database.connections.mysql.username");
		$password = config("database.connections.mysql.password");
		$dbname = config("database.connections.mysql.database");
		
		// Get a connection to the database
		$db = new PDO("mysql:host=$servername;port=$port;dbname=$dbname", $username, $password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		// Create a Security Service DAO with this connection and attempt to locate user
		$service = new SecurityDAO($db);
		$flag = $service->findByUser($user);
		
		// Close the Database connection
		$db = null;
		
		// Debugging
		Log::info("Exit SecurityService.login() with " . $flag);
		
		return $flag;
	}
}

