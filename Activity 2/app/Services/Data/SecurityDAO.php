<?php

namespace App\Services\Data;

use App\Models\UserModel;
use Illuminate\Support\Facades\Log;
use App\Services\Utility\DatabaseException;
use PDOException;

class SecurityDAO 
{
	private $db = null;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function find()
	{
		
	}
	
	public function findByUser(UserModel $user)
	{
		Log::info("Entering SecurityDAO.findByUser()...");
		try
		{
			// get required params from the User class
			$name = $user->getUsername();
			$pw = $user->getPassword();
			
			// build SQL statement
			$sql = <<<ML
				SELECT user_id, username, password
				FROM `users`
				WHERE username = :username AND password = :password
				;
ML;
			/** @var $stmt \PDOStatement */
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':username', $name);
			$stmt->bindParam(':password', $pw);
			$stmt->execute();
			
			if ($stmt->rowCount() == 1)
			{
				Log::info("Exit SecurityDAO.findByUser() with true");
				return true;
			}
			else
			{
				Log::info("Exit SecurityDAO.findByUser() with false");
				return false;
			}
		}
		catch(PDOException $e)
		{
			Log::error("Exception: ", array("message" => $e->getMessage()));
			throw new DatabaseException("Database Exception: " . $e->getMessage(), 0, $e);
		}
	}
	
	public function insert()
	{
		
	}
	
	public function update()
	{
		
	}
	
	public function delete()
	{
		
	}
}

