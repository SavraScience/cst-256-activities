<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WhatsMyNameController extends Controller
{
    public function index(Request $request)
    {
    	// display form data
    	$firstName = $request->input('firstname');
    	$lastName = $request->input('lastname');
    	
    	// Render a response View and pass the form data to it
    	$data = ['firstName' => $firstName, 'lastName' => $lastName];
    	return view('thatsme')->with($data);
	}
}
